<?php
use Illuminate\Support\Facades\Broadcast;


Broadcast::channel('USER.{id}', function ($user, $id) {
    return $user->id == $id;
});