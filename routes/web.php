<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/10/2020
 */

use Illuminate\Support\Facades\Route;

Route::get('/status/{user}', 'RealtimeController@userStatus');
Route::any('/calling', 'CallingController@connectTo')->name('connectTo');
Route::any('/cancelCall', 'CallingController@cancelConnecting')->name('cancelConnecting');
