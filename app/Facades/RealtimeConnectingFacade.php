<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/10/2020
 */

namespace Modules\Realtime\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Realtime\Services\RealtimeConnecting;

class RealtimeConnectingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return RealtimeConnecting::class;
    }
}