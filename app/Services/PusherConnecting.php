<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/10/2020
 */

namespace Modules\Realtime\Services;


use Pusher\Pusher;

class PusherConnecting extends Pusher implements RealtimeConnectingInterFace
{
    public function __construct()
    {
        $config = config('modules_realtime.pusher');

        parent::__construct($config['key'], $config['secret'], $config['app_id'], $config['options']);
    }

}