<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/10/2020
 */

namespace Modules\Realtime\Services;

interface RealtimeConnectingInterFace
{
    public function getSettings();

    public function set_logger($logger);

    public function trigger($channels, $event, $data, $socket_id = null, $debug = false, $already_encoded = false);

    public function triggerBatch($batch = array(), $debug = false, $already_encoded = false);

    public function get_channel_info($channel, $params = array());

    public function get_channels($params = array());

    public function get_users_info($channel);

    public function get($path, $params = array());

    public function socket_auth($channel, $socket_id, $custom_data = null);

    public function presence_auth($channel, $socket_id, $user_id, $user_info = null);

    public function notify($interests, $data = array(), $debug = false);

    public function webhook($headers, $body);

    public function ensure_valid_signature($headers, $body);
}