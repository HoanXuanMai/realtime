<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/12/2020
 */

namespace Modules\Realtime\Events;


use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class QikrecCalling implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    public function broadcastWith()
    {
        return $this->data;
    }

    public function broadcastAs()
    {
        return 'qikrec_calling';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $senderId = $this->data['receiver']['id'];
        return new PrivateChannel('USER.'.$senderId);
    }
}