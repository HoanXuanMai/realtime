<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/10/2020
 */

namespace Modules\Realtime\Http\Controllers;


use App\User;
use Illuminate\Routing\Controller;
use Modules\Realtime\Events\QikrecCalling;
use Modules\Realtime\Services\RealtimeConnecting;
use Modules\Realtime\Services\RealtimeConnectingInterFace;


class CallingController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    function connectTo(RealtimeConnectingInterFace $realtimeConnecting)
    {
        $socket = request('socket');
        $receiver = User::find(request('receiver'));
        if ($receiver && $socket) {
            $data = [
                'type' => 'connecting_invite',
                'sender' =>auth()->user()->toArray(),
                'receiver' =>$receiver->toArray()
            ];
            $data['sender']['socket'] = $socket;
            event(new QikrecCalling($data));
            return response([], 200);
        }
        return response([], 400);
    }

    function cancelConnecting() {
        $socket = request('socket');
        $receiver = User::find(request('receiver'));
        if ($receiver && $socket) {
            $data = [
                'type' => 'connecting_cancel',
                'sender' =>auth()->user()->toArray(),
                'receiver' =>$receiver->toArray()
            ];
            $data['sender']['socket'] = $socket;
            event(new QikrecCalling($data));
            return response([], 200);
        }
        return response([], 400);
    }
    function index(RealtimeConnectingInterFace $realtimeConnecting)
    {
        $this->connectTo();
    }
}