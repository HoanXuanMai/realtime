<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 6/1/2020
 */

namespace Modules\Realtime\Http\Controllers;


use App\User;
use Modules\Realtime\Services\RealtimeConnectingInterFace;

class RealtimeController
{

    function userStatus($user, RealtimeConnectingInterFace $connectingInterFace)
    {
        $user = User::find($user);

        try {
            $channel = $connectingInterFace->get_channel_info('private-USER.'.$user->id);
            $count = optional($channel)->subscription_count;
        } catch (\Exception $exception) {
            $count = 0;
        }

        return response()->json(['online' => (bool)$count], 200);
    }
}