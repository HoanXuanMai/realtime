<?php
/**
 * Created by HoanXuanMai
 * Project: doctor
 * Email: hoanxuanmai@gmail.com
 * Date: 5/10/2020
 */

namespace Modules\Realtime\Providers;


use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends \Illuminate\Foundation\Support\Providers\RouteServiceProvider
{

    protected $namespace = 'Modules\\Realtime\\Http\\Controllers';

    public function map()
    {
        Route::middleware(['web'])
            ->namespace($this->namespace)
            ->prefix("realtime")
            ->as("realtime.")
            ->group(__DIR__ . '/../../routes/web.php');
    }
}