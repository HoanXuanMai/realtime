<?php
/**
 * Created by HoanXuanMai
 * Project: docter
 * Email: hoanxuanmai@gmail.com
 * Date: 5/9/2020
 */

namespace Modules\Realtime\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;
use Modules\Realtime\Services\PusherConnecting;
use Modules\Realtime\Services\RealtimeConnecting;
use Modules\Realtime\Services\RealtimeConnectingInterFace;
use Pusher\Pusher;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'./../../config/modules_realtime.php' => config_path('modules_realtime.php'),
        ]);

        Broadcast::routes();
        require __DIR__ .'./../../routes/channels.php';
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'./../../config/modules_realtime.php', 'modules_realtime'
        );

        $this->app->singleton(RealtimeConnectingInterFace::class, function () {
            return new PusherConnecting();
        });

    }

}